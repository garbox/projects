<?php 
//include files
include '../class.php';
include '../email_template/temps.php';
include '../../../onlinetraining/includes/config.php';

$conn = pmimain_connect();

class regional_data_classes{
    function south(){

    $select = "SELECT Programs.PrgTopic1 AS CataID, New_Program_Names.PrnName AS Name
    FROM  Programs 
    LEFT JOIN 
    Client_Addresses 
    ON Programs.PrgLocationID = Client_Addresses.CadID
    LEFT JOIN 
    New_Program_Names
    ON Programs.PrgTopic1 = New_Program_Names.PrnCode
    LEFT JOIN Program_Agreement
    ON Programs.PrgID = Program_Agreement.PagPrgID
    WHERE DATE(Programs.PrgDate) > DATE(CURRENT_DATE()) 
    AND PrgTopic1 NOT LIKE ('A-%') 
    AND PrgTopic1 NOT LIKE ('DL-%') 
    AND PrgTopic1 NOT LIKE ('CONF%') 
    AND PrgTopic1 NOT LIKE ('SP-%')
    AND PrgTopic1 NOT LIKE ('EXAM%')
    AND Client_Addresses.CadState IN('TX','OK','LA', 'AR')
    AND Program_Agreement.PagInHouse !=1
    GROUP BY Programs.PrgTopic1
    ORDER BY New_Program_Names.PrnName ASC";


    $result  = $GLOBALS['conn']->query($select);
    
    if($result->num_rows >0){
        
        while($data = $result->fetch_object()){
            //get program info from cataID and check in region. 
            $cata_select = "SELECT Client_Addresses.CadCity AS City, ProgramDaySchedule.pdsDate AS Date
                            FROM Programs
                            LEFT JOIN 
                            Client_Addresses
                            ON Programs.PrgLocationID = Client_Addresses.CadID
                            LEFT JOIN 
                            ProgramDaySchedule
                            ON Programs.PrgID = ProgramDaySchedule.pdsPrgID
                            LEFT JOIN 
                            Program_Agreement
                            ON Programs.PrgID = Program_Agreement.PagPrgID
                            WHERE ProgramDaySchedule.pdsDate BETWEEN DATE(CURRENT_DATE()) AND DATE_ADD(CURRENT_DATE(), INTERVAL 4 MONTH)
                            AND Client_Addresses.CadState IN('TX','OK','LA', 'AR')
                            AND Programs.PrgTopic1 = '".$data->CataID."'
                            AND Program_Agreement.PagInHouse !=1
                            GROUP BY Programs.PrgID
                            ORDER BY ProgramDaySchedule.pdsDate ASC";

            $result_final = $GLOBALS['conn']->query($cata_select);
            
            // get dates for the class.
            while($data_final = $result_final->fetch_object()){
                $dates .= ucwords(strtolower($data_final->City)).", ". date("F d", strtotime($data_final->Date))."<br>"; 
            }
            
            //if there are dates... display class. and clear dates when finished. 
            if(!empty($dates)){
            ?>
            <a href="https://www.pmimd.com/programs/<?php echo $data->CataID?>.asp"><?php echo $data->Name?></a><Br>
            <?php echo $dates?><Br>
            <?

            $dates = "";
                    }

        }
    }
    else{
        echo "Classes Coming Soon!";
    }
}
    function central(){


        $select = "SELECT Programs.PrgTopic1 AS CataID, New_Program_Names.PrnName AS Name
        FROM  Programs 
        LEFT JOIN 
        Client_Addresses 
        ON Programs.PrgLocationID = Client_Addresses.CadID
        LEFT JOIN 
        New_Program_Names
        ON Programs.PrgTopic1 = New_Program_Names.PrnCode
        LEFT JOIN Program_Agreement
        ON Programs.PrgID = Program_Agreement.PagPrgID
        WHERE DATE(Programs.PrgDate) > DATE(CURRENT_DATE()) 
        AND PrgTopic1 NOT LIKE ('A-%') 
        AND PrgTopic1 NOT LIKE ('DL-%') 
        AND PrgTopic1 NOT LIKE ('CONF%') 
        AND PrgTopic1 NOT LIKE ('SP-%')
        AND PrgTopic1 NOT LIKE ('EXAM%')
        AND Client_Addresses.CadState IN('ND','SD','NE','KS','MN','IA','MO','MS','AL','IL','WI','MI','IN','KY','TN','OH')
        AND Program_Agreement.PagInHouse !=1
        GROUP BY Programs.PrgTopic1
        ORDER BY New_Program_Names.PrnName ASC";


        $result  = $GLOBALS['conn']->query($select);

        if($result->num_rows >0){
            while($data = $result->fetch_object()){
                $cata_select = "SELECT Client_Addresses.CadCity AS City, ProgramDaySchedule.pdsDate AS Date
                                FROM Programs
                                LEFT JOIN 
                                Client_Addresses
                                ON Programs.PrgLocationID = Client_Addresses.CadID
                                LEFT JOIN 
                                ProgramDaySchedule
                                ON Programs.PrgID = ProgramDaySchedule.pdsPrgID
                                LEFT JOIN 
                                Program_Agreement
                                ON Programs.PrgID = Program_Agreement.PagPrgID
                                WHERE ProgramDaySchedule.pdsDate BETWEEN DATE(CURRENT_DATE()) AND DATE_ADD(CURRENT_DATE(), INTERVAL 4 MONTH)
                                AND Client_Addresses.CadState IN('ND','SD','NE','KS','MN','IA','MO','MS','AL','IL','WI','MI','IN','KY','TN','OH')
                                AND Programs.PrgTopic1 = '".$data->CataID."'
                                AND Program_Agreement.PagInHouse !=1
                                GROUP BY Programs.PrgID
                                ORDER BY ProgramDaySchedule.pdsDate ASC";


                $result_final = $GLOBALS['conn']->query($cata_select);

                while($data_final = $result_final->fetch_object()){
                    $dates .= ucwords(strtolower($data_final->City)).", ". date("F d", strtotime($data_final->Date))."<br>"; 
                }
                if(!empty($dates)){
                ?>
                <a href="https://www.pmimd.com/programs/<?php echo $data->CataID?>.asp"><?php echo $data->Name?></a><Br>
                <?php echo $dates?><Br>
                <?

                $dates = "";
                        }

            }
        }
        else{
            echo "Classes Coming Soon";
        }
    }
    function east(){

        $select = "SELECT Programs.PrgTopic1 AS CataID, New_Program_Names.PrnName AS Name
        FROM  Programs 
        LEFT JOIN 
        Client_Addresses 
        ON Programs.PrgLocationID = Client_Addresses.CadID
        LEFT JOIN 
        New_Program_Names
        ON Programs.PrgTopic1 = New_Program_Names.PrnCode
        LEFT JOIN Program_Agreement
        ON Programs.PrgID = Program_Agreement.PagPrgID
        WHERE DATE(Programs.PrgDate) > DATE(CURRENT_DATE()) 
        AND PrgTopic1 NOT LIKE ('A-%') 
        AND PrgTopic1 NOT LIKE ('DL-%') 
        AND PrgTopic1 NOT LIKE ('CONF%') 
        AND PrgTopic1 NOT LIKE ('SP-%')
        AND PrgTopic1 NOT LIKE ('EXAM%')
        AND Client_Addresses.CadState IN('FL','GA','NC','SC','VA','WV','MD','DE','NJ','PA','NY','MA','CT','RI','VT','NH','ME')
        AND Program_Agreement.PagInHouse !=1
        GROUP BY Programs.PrgTopic1
        ORDER BY New_Program_Names.PrnName ASC";


        $result  = $GLOBALS['conn']->query($select);
        if($result->num_rows >0){
            while($data = $result->fetch_object()){
                $cata_select = "SELECT Client_Addresses.CadCity AS City, ProgramDaySchedule.pdsDate AS Date
                                FROM Programs
                                LEFT JOIN 
                                Client_Addresses
                                ON Programs.PrgLocationID = Client_Addresses.CadID
                                LEFT JOIN 
                                ProgramDaySchedule
                                ON Programs.PrgID = ProgramDaySchedule.pdsPrgID
                                LEFT JOIN 
                                Program_Agreement
                                ON Programs.PrgID = Program_Agreement.PagPrgID
                                WHERE ProgramDaySchedule.pdsDate BETWEEN DATE(CURRENT_DATE()) AND DATE_ADD(CURRENT_DATE(), INTERVAL 4 MONTH)
                                AND Client_Addresses.CadState IN('FL','GA','NC','SC','VA','WV','MD','DE','NJ','PA','NY','MA','CT','RI','VT','NH','ME')
                                AND Programs.PrgTopic1 = '".$data->CataID."'
                                AND Program_Agreement.PagInHouse !=1
                                GROUP BY Programs.PrgID
                                ORDER BY ProgramDaySchedule.pdsDate ASC";

                $result_final = $GLOBALS['conn']->query($cata_select);

                while($data_final = $result_final->fetch_object()){
                    $dates .= ucwords(strtolower($data_final->City)).", ". date("F d", strtotime($data_final->Date))."<br>"; 
                }
                if(!empty($dates)){
                ?>
                <a href="https://www.pmimd.com/programs/<?php echo $data->CataID?>.asp"><?php echo $data->Name?></a><Br>
                <?php echo $dates?><Br>
                <?

                $dates = "";
                        }

            }
        }
        else{
            echo "Classes Coming Soon!";
        }
    }

    function west(){

        $select = "SELECT Programs.PrgTopic1 AS CataID, New_Program_Names.PrnName AS Name
        FROM  Programs 
        LEFT JOIN 
        Client_Addresses 
        ON Programs.PrgLocationID = Client_Addresses.CadID
        LEFT JOIN 
        New_Program_Names
        ON Programs.PrgTopic1 = New_Program_Names.PrnCode
        LEFT JOIN Program_Agreement
        ON Programs.PrgID = Program_Agreement.PagPrgID
        WHERE DATE(Programs.PrgDate) > DATE(CURRENT_DATE()) 
        AND PrgTopic1 NOT LIKE ('A-%') 
        AND PrgTopic1 NOT LIKE ('DL-%') 
        AND PrgTopic1 NOT LIKE ('CONF%') 
        AND PrgTopic1 NOT LIKE ('SP-%')
        AND PrgTopic1 NOT LIKE ('EXAM%')
        AND Client_Addresses.CadState IN('WA','OR','CA','AR','ID','MT','NV','UT','CO','WY','AR','NM')
        AND Program_Agreement.PagInHouse !=1
        GROUP BY Programs.PrgTopic1
        ORDER BY New_Program_Names.PrnName ASC";


        $result  = $GLOBALS['conn']($select);
        if($result->num_rows >0){
            while($data = $result->fetch_object()){
                $cata_select = "SELECT Client_Addresses.CadCity AS City, ProgramDaySchedule.pdsDate AS Date
                                FROM Programs
                                LEFT JOIN 
                                Client_Addresses
                                ON Programs.PrgLocationID = Client_Addresses.CadID
                                LEFT JOIN 
                                ProgramDaySchedule
                                ON Programs.PrgID = ProgramDaySchedule.pdsPrgID
                                LEFT JOIN 
                                Program_Agreement
                                ON Programs.PrgID = Program_Agreement.PagPrgID
                                WHERE ProgramDaySchedule.pdsDate BETWEEN DATE(CURRENT_DATE()) AND DATE_ADD(CURRENT_DATE(), INTERVAL 4 MONTH)
                                AND Client_Addresses.CadState IN('WA','OR','CA','AR','ID','MT','NV','UT','CO','WY','AR','NM')
                                AND Programs.PrgTopic1 = '".$data->CataID."'
                                AND Program_Agreement.PagInHouse !=1
                                GROUP BY Programs.PrgID
                                ORDER BY ProgramDaySchedule.pdsDate ASC";

                $result_final = $GLOBALS['conn']->query($cata_select);

                while($data_final = $result_final->fetch_object()){
                    $dates .= ucwords(strtolower($data_final->City)).", ". date("F d", strtotime($data_final->Date))."<br>"; 
                }
                if(!empty($dates)){
                ?>
                <a href="https://www.pmimd.com/programs/<?php echo $data->CataID?>.asp"><?php echo $data->Name?></a><Br>
                <?php echo $dates?><Br>
                <?

                $dates = "";
                        }

            }
        }
        else{
            echo "Classes Coming Soon!";
        }
    }
    function webinars(){

        $query = "SELECT * FROM webinar
        WHERE  (date > CURRENT_DATE()) AND (program_code != 'A-TA')
        GROUP BY AF
        ORDER BY date ASC";
        
        $data = $GLOBALS['conn']->query($query);
        $rows =$data->num_rows;
        $split = ($rows/2);
        $i = 0;
            while($webinar_data = $data->fetch_object()){
                if($i <= $split){
                    $web_array[0] .="<a href='https://www.pmimd.com/programs/a-wcdaudio.asp'>".$webinar_data->name."</a><br>".date('F j', strtotime($webinar_data->date))."<br><br>";
                    $i++;
                }
                else{
                    $web_array[1] .= "<a href='https://www.pmimd.com/programs/a-wcdaudio.asp'>".$webinar_data->name."</a><br>".date('F j', strtotime($webinar_data->date))."<br><br>";
                    $i++;
                }
            }
        return $web_array;
        
    }
}


$class_obj = new regional_data_classes();
$data = $class_obj->webinars();

echo $data;

/*
// fwr class 
$mc_obj = new Mailchimp;
$template = new email_template;

// Get data from form, and explode AF data.
$data = $_REQUEST;
$AF_array = explode(" ",$data['ActionForm']);
$city = $data['city'];
$new_array = SQL_where_statment($AF_array);
$class_html = Display_classes($new_array);

$create_campaign = json_decode($mc_obj->CreateCampiagn($data), true);
$get_cam_id = json_decode($mc_obj->getInfo('campaigns/'.$create_campaign['id']), true);
$html_data = $template->target_email($class_html,$city);
$put_campaign = json_decode($mc_obj->put_campaign('campaigns/'.$get_cam_id['id'].'/content', $html_data),true);
*/

?>
